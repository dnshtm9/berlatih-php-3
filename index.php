<?php
    require('animal.php');
    require('ape.php');
    require('frog.php');

    $sheep = new Animal('shaun');
    echo "Name: $sheep->name<br>"; // "shaun"
    echo "Legs: $sheep->legs<br>"; // 4
    echo "Cold-blooded: $sheep->cold_blooded<br><br>"; // "no"

    $frog = new Frog('buduk');
    echo "Name: $frog->name<br>";
    echo "Legs: $frog->legs<br>";
    echo "Cold-blooded: $frog->cold_blooded<br>";
    echo $frog->jump() . "<br><br>";

    $ape = new Ape('kera sakti');
    echo "Name: $ape->name<br>";
    echo "Legs: $ape->legs<br>";
    echo "Cold-blooded: $ape->cold_blooded<br>";
    echo $ape->yell() . "<br><br>";
?>